const colors = {
  purple: '#3646A1',
  white: '#fff',
  green: '#4A9259',
};
export default colors;
