import {StyleSheet, Dimensions} from 'react-native';
import colors from './colors';

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  //Other
  appContainer: {
    width: width,
    height: height,
    backgroundColor: colors.purple,
  },
  topBar: {
    flexDirection: 'row',
    position: 'absolute',
    top: 10,
    width: '80%',
    height: 50,
    borderRadius: 25,
    marginHorizontal: '10%',
    paddingRight: 10,
    paddingLeft: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.green,
  },
  bottomBar: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 40,
    height: 50,
    borderRadius: 25,
    width: '60%',
    marginHorizontal: '20%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  startButton: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: height * 0.2,
    width: '80%',
    height: 50,
    borderRadius: 25,
    marginHorizontal: '10%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.green,
  },
  topBarText: {
    fontSize: 15,
    color: colors.white,
    marginLeft: 5,
  },
  nameText: {
    fontSize: 20,
    color: colors.white,
    marginLeft: 15,
  },
  startText: {
    fontSize: 20,
    color: colors.white,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
export default styles;