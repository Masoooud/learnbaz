/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View, TouchableOpacity} from 'react-native';
import styles from '../style/styles';
import colors from '../style/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {showToast} from './ShowToast';

class BottomBar extends React.Component {
  render() {
    return (
      <View style={styles.bottomBar}>
        <TouchableOpacity onPress={() => showToast('Setting clicked!')}>
          <Ionicons name="ios-settings" size={30} color={colors.white} />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => showToast('Shop clicked!')}>
          <MaterialCommunityIcons
            name="shopping"
            size={27}
            color={colors.white}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => showToast('Chat clicked!')}>
          <MaterialIcons name="chat" size={25} color={colors.white} />
        </TouchableOpacity>
      </View>
    );
  }
}

export default BottomBar;
