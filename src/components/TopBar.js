/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import styles from '../style/styles';
import colors from '../style/colors';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {showToast} from './ShowToast';
const {width, height} = Dimensions.get('window');

class TopBar extends React.Component {
  render() {
    return (
      <View style={styles.topBar}>
        <TouchableOpacity onPress={() => showToast('Profile clicked!')}>
          <View style={styles.row}>
            <FontAwesome name="user-circle-o" size={40} color={colors.white} />
            <Text style={styles.nameText}>Masoud</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.row}>
          <View style={[styles.row, {marginHorizontal: 5}]}>
            <FontAwesome5 name="medal" size={20} color={colors.white} />
            <Text style={styles.topBarText}>1</Text>
          </View>
          <View style={[styles.row, {marginHorizontal: 5}]}>
            <FontAwesome name="diamond" size={20} color={colors.white} />
            <Text style={styles.topBarText}>1</Text>
          </View>
          <View style={[styles.row, {marginHorizontal: 5}]}>
            <MaterialCommunityIcons
              name="treasure-chest"
              size={30}
              color={colors.white}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default TopBar;
